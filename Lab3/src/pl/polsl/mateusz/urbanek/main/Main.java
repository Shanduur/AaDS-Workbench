package pl.polsl.mateusz.urbanek.main;

import pl.polsl.mateusz.urbanek.hashing.numbers.*;
import pl.polsl.mateusz.urbanek.hashing.words.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        double[] filling = {0.5, 0.6, 0.7, 0.8, 0.9};
        long[] dataArr = new long[1050000];
        long[] missArr = new long[1050000];
        int i = 0;
        try {
            // this is renamed file containing 1050000 lines.
            Scanner scanner = new Scanner(new File("C:\\Repos\\Data\\num.txt"));
            while (scanner.hasNextLong()) {
                dataArr[i] = scanner.nextLong();
                i++;
            }
            scanner.close();

            i = 0;
            // and this is renamed miss file.
            Scanner scanner2 = new Scanner(new File("C:\\Repos\\Data\\miss.txt"));
            while (scanner2.hasNextLong()) {
                missArr[i] = scanner2.nextLong();
                i++;
            }
            scanner2.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        for (double d: filling) {
            System.out.println("\nFilling: " + d);

            AbstractHashingInt iLinear = new LinearHashingInt(d);
            System.out.println(iLinear.getClass());
            iLinear.fillArray(dataArr);
            iLinear.searchHit(dataArr);
            iLinear.searchMiss(missArr);

            AbstractHashingInt iDouble = new DoubleHashingInt(d);
            System.out.println("\n" + iDouble.getClass());
            iDouble.fillArray(dataArr);
            iDouble.searchHit(dataArr);
            iDouble.searchMiss(missArr);

            AbstractHashingString sLinear = new LinearHashingString(d);
            System.out.println("\n" + sLinear.getClass());
            sLinear.fillArray(dataArr, longToStringArr(dataArr));
            sLinear.searchHit(dataArr);
            sLinear.searchMiss(missArr);

            AbstractHashingString sDouble = new DoubleHashingString(d);
            System.out.println("\n" + sDouble.getClass());
            sDouble.fillArray(dataArr, longToStringArr(dataArr));
            sDouble.searchHit(dataArr);
            sDouble.searchMiss(missArr);
        }
    }

    private static String[] longToStringArr(long[] arr) {
        String[] strings = new String[arr.length];

        for (int i = 0; i < strings.length; i++) {
            strings[i] = Long.toString(arr[i]);
        }

        return strings;
    }
}
