package pl.polsl.mateusz.urbanek.hashing.numbers;

public class DoubleHashingInt extends AbstractHashingInt {

    public DoubleHashingInt(double percentFill){
        super(percentFill);
    }

    @Override
    public long hash(long key) {
        return (key % arraySize);
    }

    public long hash2(long key) {
        return (key % (arraySize-1))+1;
    }

    @Override
    public long hashCode(long key, int index){
        return ((hash(key)+index* hash2(key)) % arraySize);
    }
}