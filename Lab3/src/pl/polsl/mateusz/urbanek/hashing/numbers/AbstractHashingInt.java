package pl.polsl.mateusz.urbanek.hashing.numbers;

public abstract class AbstractHashingInt {
    public int arraySize;
    public double testSize;
    public long[] array;
    public double percentFill;

    int counter = 0;

    AbstractHashingInt(double percentFill) {
        this.arraySize = 1000003;
        this.array = new long[arraySize];
        this.testSize = (arraySize*percentFill)/10;
        this.percentFill = percentFill;
    }


    public abstract long hash(long key);
    public abstract long hashCode(long key, int index);


    public void  insert(long additionalValue){
        for(int i = 0; i < arraySize; i++) {

            int index = (int) (hashCode(additionalValue, i));
            if (array[index] == 0) {
                array[index] = additionalValue;
                break;
            }
        }
    }


    public void fillArray(long[] inputArray){
        for(int i=0; i < arraySize ; i++)
            array[i] = 0;

        for(int i=0; i < (arraySize*percentFill); i++)
            insert(inputArray[i]);
    }


    public void search(long value) {

        for(int i = 0; i < arraySize; i++){

            int index =(int)(hashCode(value, i));
            counter++;
            if(array[index] == value){
                break;
            }
            else if(array[index]==0){
                break;
            }
        }
    }


    public void searchHit(long[] inputArray){
        for(int i=0;i < arraySize*percentFill;i+=(percentFill*10)){
            search(inputArray[i]);
        }

        System.out.println("Hit:"+counter/testSize);
        counter = 0;
    }

    public void searchMiss(long[] inputArray){
        for(int i=0;i < arraySize*percentFill;i+=(percentFill*10)){
            search(inputArray[i]);
        }

        System.out.println("Miss:"+(counter)/testSize);
        counter = 0;
    }

}