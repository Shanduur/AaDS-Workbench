package pl.polsl.mateusz.urbanek.hashing.numbers;

public class LinearHashingInt extends AbstractHashingInt {

    public LinearHashingInt(double percentFill){
        super(percentFill);
    }

    @Override
    public long hash(long key) {
        return (key % arraySize);
    }

    @Override
    public long hashCode(long key, int index) {
        return ((hash(key)+index) % arraySize);
    }

}
