package pl.polsl.mateusz.urbanek.hashing.words;

public abstract class AbstractHashingString {
    public int arraySize;
    public double testSize;
    private HashNode[] array;
    public double percentFill;

    int counter = 0;

    AbstractHashingString(double percentFill){
        this.arraySize = 1000003;
        this.array = new HashNode[arraySize];
        this.testSize = (arraySize*percentFill)/10;
        this.percentFill = percentFill;
    }


    public abstract long hash(long x);
    public abstract long hashCode(long x, int index);


    public void  insert(long key, String additionalValue){
        for(int i = 0; i < arraySize; i++) {

            int index = (int) (hashCode(key, i));
            if (array[index].getKey() == 0) {
                array[index].setKey(key);
                array[index].setValue(additionalValue);
                break;
            }
        }
    }


    public void fillArray(long[] keys, String[] values) {
        for(int i = 0; i < arraySize ; i++)
            array[i] = new HashNode();

        for(int i = 0; i < (arraySize*percentFill); i++)
            insert(keys[i], values[i]);
    }


    public void search(long key) {
        for(int i = 0; i< arraySize; i++) {

            int index =(int)(hashCode(key, i));
            counter++;

            if(array[index].getKey() == key) {
                break;
            } else if(array[index].getKey() == 0) {
                break;
            }

        }
    }


    public void searchHit(long[] keys){
        for(int i = 0; i < arraySize * percentFill; i += (percentFill*10)){
            search(keys[i]);
        }

        System.out.println("Hit:" + counter/testSize);
        counter = 0;
    }

    public void searchMiss(long[] keys){
        for(int i = 0; i < arraySize * percentFill; i += (percentFill*10)){
            search(keys[i]);
        }

        System.out.println("Miss:"+(counter)/testSize);
        counter = 0;
    }

}