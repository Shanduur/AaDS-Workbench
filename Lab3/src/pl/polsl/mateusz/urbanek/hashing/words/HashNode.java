package pl.polsl.mateusz.urbanek.hashing.words;

public class HashNode {
    private long key;
    private String value;

    HashNode(long key, String value) {
        this.key = key;
        this.value = value;
    }

    public void setKey(long key) {
        this.key = key;
    }

    public void setValue(String value) {
        this.value = value;
    }

    HashNode() {
        this.key = 0;
        this.value = "";
    }

    public long getKey() {
        return key;
    }

    public String getValue() {
        return value;
    }
}
