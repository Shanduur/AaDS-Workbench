package pl.polsl.mateusz.urbanek.hashing.words;

public class LinearHashingString extends AbstractHashingString {

    public LinearHashingString(double percentFill) {
        super(percentFill);
    }

    @Override
    public long hash(long x) {
        return (x % arraySize);
    }

    @Override
    public long hashCode(long x, int index){
        return ((hash(x)+index) % arraySize);
    }
}