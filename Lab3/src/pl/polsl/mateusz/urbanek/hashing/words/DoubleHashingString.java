package pl.polsl.mateusz.urbanek.hashing.words;

public class DoubleHashingString extends AbstractHashingString {

    public DoubleHashingString(double percentFill) {
        super(percentFill);
    }

    @Override
    public long hash(long x) {
        return (x % arraySize);
    }

    public long hash2(long x) {
        return (x % (arraySize-1))+1;
    }

    @Override
    public long hashCode(long x, int index){
        return ((hash(x)+index* hash2(x)) % arraySize);
    }
}