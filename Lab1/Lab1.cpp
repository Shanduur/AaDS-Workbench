#define _CRT_SECURE_NO_WARNINGS

#include <iostream>
#include <chrono>
#include <time.h>
#include <fstream>
#include <string>

#include "intSort.h"
#include "stringSort.h"

#define SIZE 500
#define SIZE_2 10

int main()
{
	long long duration[3][6];
	duration[0][5] = 0;
	duration[1][5] = 0;
	duration[2][5] = 0;

	for (int i = 0; i < 3; i++)
	{
		int a[SIZE];
		int arr[SIZE];
		int n = SIZE;

		srand(time(NULL));

		for (int i = 0; i < n; i++)
		{
			a[i] = rand();
		}

		for (int i = 0; i < SIZE; i++) {
			arr[i] = a[i];
		}
		auto t1 = std::chrono::high_resolution_clock::now();
		bubbleSort(arr, n);
		auto t2 = std::chrono::high_resolution_clock::now();

		duration[i][0] = std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count();

		for (int i = 0; i < SIZE; i++) {
			arr[i] = a[i];
		}
		t1 = std::chrono::high_resolution_clock::now();
		bubbleSortImproved(arr, n);
		t2 = std::chrono::high_resolution_clock::now();

		duration[i][1] = std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count();

		for (int i = 0; i < SIZE; i++) {
			arr[i] = a[i];
		}
		t1 = std::chrono::high_resolution_clock::now();
		selectionSort(arr, n);
		t2 = std::chrono::high_resolution_clock::now();

		duration[i][2] = std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count();
		
		for (int i = 0; i < SIZE; i++) {
			arr[i] = a[i];
		}
		t1 = std::chrono::high_resolution_clock::now();
		selectionSortImproved(arr, n);
		t2 = std::chrono::high_resolution_clock::now();

		duration[i][3] = std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count();

		for (int i = 0; i < SIZE; i++) {
			arr[i] = a[i];
		}
		t1 = std::chrono::high_resolution_clock::now();
		insertionSort(arr, n);
		t2 = std::chrono::high_resolution_clock::now();

		duration[i][4] = std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count();
	}

	std::cout << SIZE << '\n' << "Bubble | Bubble+ | Selection | Selection+ | Insertion | Insertion+" << std::endl;

	for (int i = 0; i < 3; i++) {
		for (int j = 0; j < 6; j++)
		{
			std::cout << duration[i][j] << '\t';
		}
		std::cout << std::endl;
	}

	std::ifstream f("C:\\Repos\\ex.txt");

	std::string s;
	char line[10][MAX];

	for (int i = 0; i < SIZE_2; i++) {
		getline(f, s);
		strcpy(line[i], s.c_str());
	}

	auto t1 = std::chrono::high_resolution_clock::now();
	bubbleSort(line, SIZE_2);
	auto t2 = std::chrono::high_resolution_clock::now();

	std::cout << std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count() << std::endl;

	t1 = std::chrono::high_resolution_clock::now();
	bubbleSortImproved(line, SIZE_2);
	t2 = std::chrono::high_resolution_clock::now();

	std::cout << std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count() << std::endl;

	t1 = std::chrono::high_resolution_clock::now();
	selectionSort(line, SIZE_2);
	t2 = std::chrono::high_resolution_clock::now();

	std::cout << std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count() << std::endl;


	t1 = std::chrono::high_resolution_clock::now();
	selectionSortImproved(line, SIZE_2);
	t2 = std::chrono::high_resolution_clock::now();

	std::cout << std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count() << std::endl;

	t1 = std::chrono::high_resolution_clock::now();
	insertionSort(line, SIZE_2);
	t2 = std::chrono::high_resolution_clock::now();

	std::cout << std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count() << std::endl;

	return 0;
}