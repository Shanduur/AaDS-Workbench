#include "intSort.h"

void swap(int* a, int* b)
{
	int* temp = a;
	a = b;
	b = temp;
}

void bubbleSort(int arr[], int n)
{
	int i, j;
	for (i = 0; i < n - 1; i++)
	{
		for (j = 0; j < n - 1; j++)
		{
			if (arr[j] > arr[j + 1])
			{
				swap(&arr[j], &arr[j + 1]);
			}
		}
	}
}

void bubbleSortImproved(int arr[], int n)
{
	int i, j;
	for (i = 0; i < n - 1; i++)
	{
		for (j = 0; j < n - i - 1; j++)
		{
			if (arr[j] > arr[j + 1])
			{
				swap(&arr[j], &arr[j + 1]);
			}
		}
	}
}

void selectionSort(int arr[], int n)
{
	for (int i = 0; i < n - 1; i++)
	{
		int index_min = i;
		int value_min = arr[i];
		for (int j = i + 1; j < n; j++)
		{
			if (arr[j] < value_min) {
				index_min = j;
				value_min = arr[j];
			}
		}
		swap(&arr[i], &arr[i + 1]);
	}
}

void selectionSortImproved(int arr[], int n)
{
	for (unsigned minIndex = 0, maxIndex = n - 1; minIndex < maxIndex;)
	{
		int min = arr[minIndex];
		int max = arr[maxIndex];
		unsigned newMinIndex = minIndex + 1;
		unsigned newMaxIndex = maxIndex;

		for (unsigned index = newMinIndex; index <= newMaxIndex;)
		{
			int value = arr[index];

			if (value <= min)
			{
				if (value < min)
				{
					min = value;
					newMinIndex = minIndex;
					arr[index] = arr[newMinIndex];
				}
				else
				{
					arr[index] = arr[newMinIndex];
					++index;
				}
				arr[newMinIndex] = value;
				++newMinIndex;
			}
			else if (value >= max)
			{
				if (value > max)
				{
					max = value;
					newMaxIndex = maxIndex;
				}
				arr[index] = arr[newMaxIndex];
				arr[newMaxIndex] = value;
				--newMaxIndex;
			}
			else ++index;
		}
		minIndex = newMinIndex;
		maxIndex = newMaxIndex;
	}
}

void insertionSort(int arr[], int n)
{
	int value_min;
	int j;

	for (int i = 1; i < n; i++)
	{
		value_min = arr[i];
		j = i - 1;

		while (j >= 0 && arr[j] > value_min)
		{
			arr[j + 1] = arr[j];
			j = j - 1;
		}
		arr[j + 1] = value_min;
	}
}