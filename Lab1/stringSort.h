#pragma once

#define _CRT_SECURE_NO_WARNINGS

#include <string>

#define MAX 64

void bubbleSort(char arr[][MAX], int n);
void bubbleSortImproved(char arr[][MAX], int n);
void selectionSort(char arr[][MAX], int n);
void selectionSortImproved(char arr[][MAX], int n);
void insertionSort(char arr[][MAX], int n);