#pragma once

#include <iostream>

void swap(int* a, int* b);
void bubbleSort(int arr[], int n);
void bubbleSortImproved(int arr[], int n);
void selectionSort(int arr[], int n);
void selectionSortImproved(int arr[], int n);
void insertionSort(int arr[], int n);