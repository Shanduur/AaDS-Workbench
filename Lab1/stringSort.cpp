#include "stringSort.h"

#include <iostream>

void bubbleSort(char arr[][MAX], int n)
{
	char tmp[MAX];

	int i, j;
	for (i = 0; i < n - 1; i++)
	{
		for (j = 0; j < n - 1; j++)
		{
			if (strcmp(arr[j], arr[j + 1]) > 0)
			{
				strcpy(tmp, arr[j]);
				strcpy(arr[j], arr[j+1]);
				strcpy(arr[j+1], tmp);
			}
		}
	}
}

void bubbleSortImproved(char arr[][MAX], int n)
{
	char tmp[MAX];

	int i, j;
	for (i = 0; i < n - 1; i++)
	{
		for (j = 0; j < n - i - 1; j++)
		{
			if (strcmp(arr[j], arr[j + 1]) > 0)
			{
				strcpy(tmp, arr[j]);
				strcpy(arr[j], arr[j+1]);
				strcpy(arr[j+1], tmp);
			}
		}
	}
}

void selectionSort(char arr[][MAX], int n)
{
	char tmp[MAX];

	for (int i = 0; i < n - 1; i++)
	{
		int index_min = i;
		char value_min[MAX];

		strcpy(value_min, arr[i]);

		for (int j = i + 1; j < n; j++)
		{
			if (strcmp(arr[j], value_min) < 0) {
				index_min = j;
				strcpy(value_min, arr[j]);
			}
		}
		strcpy(tmp, arr[i+1]);
		strcpy(arr[i+1], arr[i]);
		strcpy(arr[i], tmp);
	}
}

void selectionSortImproved(char arr[][MAX], int n)
{
	for (unsigned minIndex = 0, maxIndex = n - 1; minIndex < maxIndex;)
	{
		char min[MAX]; 
		strcpy(min, arr[minIndex]);
		char max[MAX];
		strcpy(max, arr[maxIndex]);
		unsigned newMinIndex = minIndex + 1;
		unsigned newMaxIndex = maxIndex;

		for (unsigned index = newMinIndex; index <= newMaxIndex;)
		{
			char value[MAX];
			strcpy(value, arr[index]);

			if (strcmp(value, min) <= 0)
			{
				if (value < min)
				{
					strcpy(min, value);
					newMinIndex = minIndex;
					strcpy(arr[index], arr[newMinIndex]);
				}
				else
				{
					strcpy(arr[index], arr[newMinIndex]);
					++index;
				}
				strcpy(arr[newMinIndex], value);
				++newMinIndex;
			}
			else if (strcmp(value, max) >= 0)
			{
				if (strcmp(value, max) > 0)
				{
					strcpy(max, value);
					newMaxIndex = maxIndex;
				}
				strcpy(arr[index], arr[newMaxIndex]);
				strcpy(arr[newMaxIndex], value);
				--newMaxIndex;
			}
			else ++index;
		}
		minIndex = newMinIndex;
		maxIndex = newMaxIndex;
	}
}

void insertionSort(char arr[][MAX], int n)
{
	char value_min[MAX];
	int j;

	for (int i = 1; i < n; i++)
	{
		strcpy(value_min, arr[i]);
		j = i - 1;

		while (j >= 0 && strcmp(arr[j], value_min) > 0)
		{
			strcpy(arr[j + 1], arr[j]);
			j = j - 1;
		}
		strcpy(arr[j + 1], value_min);
	}
}