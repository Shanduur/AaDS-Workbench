# Algorithms and Data Structures Laboratory 2
## Fast Sorting algorithms

Final source code|written in `Java` is placed inside `./FinishedCode-Java/src/main/`

I encountered problem during developing code in `C++` and after some time wasted for debugging I abandoned the idea of writing code here. I also attached source inside `./FailedCode-CPP/` directory as additional resource.

Final results and data used as string dictionary is placed insie `./Data/` directory.

## Results

### Integers

|Size of data array|Shell Sort|Quick Sort|Median Quick Sort|Quick And Insertion Sort|
|---|---|---|---|---|
|**128000**|54292700|23388000|13218700|11241000|
|**512000**|389376400|56080900|56113400|46501500|
|**1024000**|1125024100|105085000|128663800|92528200|
|**2048000**|2871771900|220424000|245995700|180917700|
|**4096000**|6592539400|501329300|539400300|389074400|

### Strings

|Size of data array|Shell Sort|Quick Sort|Median Quick Sort|Quick And Insertion Sort|
|---|---|---|---|---|
|**16000**|16063100|14066600|10792100|7212400|
|**32000**|3564800|13575300|17616400|12628200|
|**64000**|10754200|27598500|29540900|28445200|
|**128000**|39640300|61535800|98275800|60093100|
|**144884**|45879400|87580400|121993900|80861200|

## Conclussions

As we can observe **Quick Sort with addition of Insertion Sort** is allways the best performer.
