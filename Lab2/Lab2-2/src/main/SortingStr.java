package main;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class SortingStr {

    public static List<String> shellSort(List<String> arr) {
        int n = arr.size();
        int h = 1;

        while (h < n/9) {
            h = 3 * h;
        }

        while (h > 0) {
            for (int i = h; i<n; i++) {
                String x = arr.get(i);
                int j = i;

                while ((j >= h) && (x.compareTo(arr.get(i)) < 0)) {
                    arr.set(j, arr.get(j-h));
                    j = j - h;
                }
                arr.set(j, x);
            }

            h = h/3;
        }

        return arr;
    }

    private static int partition(List<String> arr, int l, int h) {
        String pivot;

        pivot = arr.get(h);

        int i = (l-1);
        for (int j=l; j<h; j++) {
            if (arr.get(j).compareTo(pivot) < 0) {
                i++;

                Collections.swap(arr, i, j);
            }
        }

        Collections.swap(arr, i+1, h);

        return i+1;
    }

    public static List<String> quickSort(List<String> arr, int l, int r) {
        if (l < r) {
            int pivot = partition(arr, l, r);

            quickSort(arr, l, pivot-1);
            quickSort(arr, pivot+1, r);
        }

        return arr;
    }

    public static int medianPivot(List<String> arr, int l, int r) {
        int mid = (r) / 2;

        List<String> sortingArr = new ArrayList<>();

        sortingArr.add(arr.get(l));
        sortingArr.add(arr.get(mid));
        sortingArr.add(arr.get(r));
        shellSort(sortingArr);

        String middleValue = sortingArr.get(1);
        String temp = arr.get(r);
        arr.set(r, middleValue);

        if (middleValue.compareTo(arr.get(l)) == 0) {
            arr.set(l, temp);
        } else if (middleValue.compareTo(arr.get(mid)) == 0) {
            arr.set(mid, temp);
        }

        return partition(arr, l, r);
    }

    public static List<String> medianQuickSort(List<String> arr, int l, int h) {
        if (l < h) {

            int pivot = medianPivot(arr, l, h);

            arr = quickSort(arr, l, h);
        }

        return arr;
    }

    public static List<String> insertionSort(List<String> arr, int low, int n)
    {
        for (int i = low + 1; i <= n; i++)
        {
            String value = arr.get(i);
            int j = i;

            while (j > low && arr.get(j - 1).compareTo(value) > 0)
            {
                arr.set(j, arr.get(j - 1));
                j--;
            }

            arr.set(j, value);
        }

        return arr;
    }

    public static List<String> quickAndInsertionSort(List<String> arr, int l, int h)
    {
        while (l < h)
        {
            if(h - l < 10)
            {
                insertionSort(arr, l, h);
                break;
            }
            else
            {
                int pivot = partition(arr, l, h);

                if (pivot - l < h - pivot) {
                    quickAndInsertionSort(arr, l, pivot - 1);
                    l = pivot + 1;
                } else {
                    quickAndInsertionSort(arr, pivot + 1, h);
                    h = pivot - 1;
                }
            }
        }

        return arr;
    }
}
