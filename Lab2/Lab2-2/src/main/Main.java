package main;

import java.io.*;
import java.util.*;

public class Main {

    private final static int bound = Integer.MAX_VALUE;
    //private final static int bound = 100;

    private final static int[] numOfIntegers = {128000, 512000, 1024000, 2048000, 4096000};
    //private final static int[] numOfIntegers = {10, 20};
    final static int[] numOfStrings = {16000, 32000, 64000, 128000, 144884};
    //final static int[] numOfStrings = {10, 15};

    private static int[] generateInt(int n) {
        Random random = new Random();

        int[] arr = new int[n];

        for (int i = 0; i < n; i++) {
            arr[i] = random.nextInt(bound);
        }

        return arr;
    }

    private static List<String> readString(int n) throws Exception {
        File file = new File("C:\\Repos\\Data\\english.txt");

        BufferedReader br = new BufferedReader(new FileReader(file));

        String st;
        List<String> arr = new ArrayList<>();
        int i = 0;
        while (((st = br.readLine()) != null) && (i < n)) {
            arr.add(st);
            i++;
        }

        Collections.shuffle(arr);

        return arr;
    }

    private static boolean isSortedInt(int[] arr)
    {
        int i = 1;
        int n = arr.length;
        boolean is_sorted = true;

        while ((i < n) && is_sorted) {
            if (arr[i - 1] > arr[i])
                is_sorted = false;
            i++;
        }
        return (is_sorted);
    }

    private static boolean isSortedStr(List<String> arr)
    {
        int i = 1;
        int n = arr.size();
        boolean is_sorted = true;

        while ((i < n) && is_sorted) {
            if (arr.get(i - 1).compareTo(arr.get(i)) > 0)
                is_sorted = false;
            i++;
        }
        return (is_sorted);
    }

    public static void main(String[] args) {
        long t1;
        long t2;

        long[][] resultsInt = new long[numOfIntegers.length][4];
        long[][] resultsStr = new long[numOfStrings.length][4];

        for (int i = 0; i < numOfIntegers.length; i++) {
            // Generate array
            int[] arr = generateInt(numOfIntegers[i]);

            // Create backup
            int[] arr_cp = arr.clone();

            System.out.println("ShellSort on int");
            t1 = System.nanoTime();
            SortingInt.shellSort(arr);
            t2 = System.nanoTime();
            resultsInt[i][0] = t2 - t1;
            System.out.println(isSortedInt(arr));

            // Restore backup
            arr = arr_cp.clone();

            System.out.println("QuickSort on int");
            t1 = System.nanoTime();
            SortingInt.quickSort(arr, 0, arr.length-1);
            t2 = System.nanoTime();
            resultsInt[i][1] = t2 - t1;
            System.out.println(isSortedInt(arr));

            // Restore backup
            arr = arr_cp.clone();

            System.out.println("MedianQuickSort on int");
            t1 = System.nanoTime();
            SortingInt.medianQuickSort(arr, 0, arr.length-1);
            t2 = System.nanoTime();
            resultsInt[i][2] = t2 - t1;
            System.out.println(isSortedInt(arr));

            // Restore backup
            arr = arr_cp.clone();

            System.out.println("QuickAndInsertionSort on int");
            t1 = System.nanoTime();
            SortingInt.quickAndInsertionSort(arr, 0, arr.length-1);
            t2 = System.nanoTime();
            resultsInt[i][3] = t2 - t1;
            System.out.println(isSortedInt(arr));
        }

        System.out.println("\n==========\n");

        List<String> arr = new ArrayList<>();
        List<String> arr_cp = new ArrayList<>();

        for (int i = 0; i < numOfStrings.length; i++) {
            try {
                arr = readString(numOfStrings[i]);
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }

            // Create backup
            arr_cp = new ArrayList<>(arr);

            // Restore backup
            arr = new ArrayList<>(arr_cp);

            System.out.println("ShellSort on str");
            t1 = System.nanoTime();
            SortingStr.shellSort(arr);
            t2 = System.nanoTime();
            resultsStr[i][0] = t2 - t1;
            System.out.println(isSortedStr(arr));

            // Restore backup
            arr = new ArrayList<>(arr_cp);

            System.out.println("QuickSort on str");
            t1 = System.nanoTime();
            SortingStr.quickSort(arr, 0, arr.size()-1);
            t2 = System.nanoTime();
            resultsStr[i][1] = t2 - t1;
            System.out.println(isSortedStr(arr));

            // Restore backup
            arr = new ArrayList<>(arr_cp);

            System.out.println("MedianQuickSort on str");
            t1 = System.nanoTime();
            SortingStr.medianQuickSort(arr, 0, arr.size()-1);
            t2 = System.nanoTime();
            resultsStr[i][2] = t2 - t1;
            System.out.println(isSortedStr(arr));

            // Restore backup
            arr = new ArrayList<>(arr_cp);

            System.out.println("QuickAndInsertionSort on str");
            t1 = System.nanoTime();
            SortingStr.quickAndInsertionSort(arr, 0, arr.size()-1);
            t2 = System.nanoTime();
            resultsStr[i][3] = t2 - t1;
            System.out.println(isSortedStr(arr));
        }

        System.out.println(Arrays.deepToString(resultsInt));
        System.out.println(Arrays.deepToString(resultsStr));

        File out = new File("C:\\Repos\\Data\\out.txt");
        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append("Generated ints:\n")
                .append(Arrays.toString(numOfIntegers)).append('\n')
                .append("Read strings:\n")
                .append(Arrays.toString(numOfStrings)).append('\n')
                .append("[ShellSort, QuickSort, MedianQuickSort, QuickAndInsertionSort] time in ns\n")
                .append("Int:\n")
                .append(Arrays.deepToString(resultsInt)).append('\n')
                .append("Str:\n")
                .append(Arrays.deepToString(resultsStr));

        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter(out));
            writer.write(stringBuilder.toString());
            writer.close();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
