package main;

import java.util.Random;

public class SortingInt {

    public static int[] shellSort(int[] arr) {
        int n = arr.length;
        int h = 1;

        while (h < n/9) {
            h = 3 * h;
        }

        while (h > 0) {
            for (int i = h; i<n; i++) {
                int x = arr[i];
                int j = i;

                while ((j >= h) && ( x < arr[j-h])) {
                    arr[j] = arr[j-h];
                    j = j - h;
                }
                arr[j] = x;
            }

            h = h/3;
        }

        return arr;
    }

    private static int partition(int[] arr, int l, int h) {
        int pivot = 0;

        pivot = arr[h];

        int i = (l-1);
        for (int j=l; j<h; j++) {
            if (arr[j] < pivot) {
                i++;

                int temp = arr[i];
                arr[i] = arr[j];
                arr[j] = temp;
            }
        }

        int temp = arr[i+1];
        arr[i+1] = arr[h];
        arr[h] = temp;

        return i+1;
    }

    public static int[] quickSort(int[] arr, int l, int r) {
        if (l < r) {
            int pivot = partition(arr, l, r);

            quickSort(arr, l, pivot-1);
            quickSort(arr, pivot+1, r);
        }

        return arr;
    }

    public static int medianPivot(int arr[], int l, int r) {
        int mid = (r) / 2;

        int[] sortingArr = { arr[l], arr[mid], arr[r] };

        shellSort(sortingArr);

        int middleValue = sortingArr[1];
        int temp = arr[r];
        arr[r] = middleValue;

        if (middleValue == arr[l]) {
            arr[l] = temp;
        } else if (middleValue == arr[mid]) {
            arr[mid] = temp;
        }

        return partition(arr, l, r);
    }

    public static int[] medianQuickSort(int[] arr, int l, int h) {
        if (l < h) {

            int pivot = medianPivot(arr, l, h);

            arr = quickSort(arr, l, h);
        }

        return arr;
    }

    private static int[] insertionSort(int[] arr, int l, int n)
    {
        for (int i = l + 1; i <= n; i++)
        {
            int value = arr[i];
            int j = i;

            while (j > l && arr[j - 1] > value)
            {
                arr[j] = arr[j - 1];
                j--;
            }

            arr[j] = value;
        }

        return arr;
    }

    public static int[] quickAndInsertionSort(int[] arr, int l, int h)
    {
        while (l < h)
        {
            if(h - l < 10)
            {
                insertionSort(arr, l, h);
                break;
            }
            else
            {
                int pivot = partition(arr, l, h);

                if (pivot - l < h - pivot) {
                    quickAndInsertionSort(arr, l, pivot - 1);
                    l = pivot + 1;
                } else {
                    quickAndInsertionSort(arr, pivot + 1, h);
                    h = pivot - 1;
                }
            }
        }

        return arr;
    }

}
