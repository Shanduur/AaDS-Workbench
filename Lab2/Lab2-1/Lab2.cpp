#define _CRT_SECURE_NO_WARNINGS

#include <iostream>
#include <chrono>
#include <time.h>
#include <fstream>
#include <algorithm>
#include <random>

#include "intSort.h"

#define SAMPLE_SIZE 5

std::chrono::steady_clock::time_point t1, t2;

int check_if_sorted(int arr[], int n) {
	int i = 1;
	int is_sorted = 1;
	while ((i < n) && is_sorted) {
		if (arr[i - 1] > arr[i])
			is_sorted = 0;
		i++;
	}
	return (is_sorted);
}

int main()
{
	long long duration[3][6];

	int length = (sizeof(duration) / sizeof(long long)) / (sizeof(duration[1]) / sizeof(long long));
	int height = sizeof(duration[1]) / sizeof(long long);

	for (int i = 0; i < length; i++)
	{
		for (int j = 0; j < height; j++)
			duration[i][j] = 0;
	}

	for (int i = 0; i < 2; i++)
	{
		std::cout << "Iteration: " << i << std::endl;

		// array on which sorting is performed
		int arr[SAMPLE_SIZE];
		// copy of array
		int arr_cp[SAMPLE_SIZE];

		int* p;

		// create backup of array
		for (int i = 0; i < SAMPLE_SIZE; i++)
		{
			arr_cp[i] = rand();
		}

		// copy it to main array
		for (int i = 0; i < SAMPLE_SIZE; i++)
		{
			arr[i] = arr_cp[i];
		}

		std::cout << "== Shellsort ==" << std::endl;

		t1 = std::chrono::high_resolution_clock::now();
		p = shellsort(arr, SAMPLE_SIZE);
		t2 = std::chrono::high_resolution_clock::now();
		duration[i][1] = std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count();

		for (int i = 0; i < SAMPLE_SIZE; i++)
		{
			std::cout << p[i] << std::endl;
		}

		if (!check_if_sorted(p, SAMPLE_SIZE)) std::cout << "wrong!" << std::endl;
		else std::cout << "ok" << std::endl;

		// restore backup
		for (int i = 0; i < SAMPLE_SIZE; i++)
		{
			arr[i] = arr_cp[i];
		}

		std::cout << "== Quicksort ==" << std::endl;

		t1 = std::chrono::high_resolution_clock::now();
		p = quicksort(arr, 0, SAMPLE_SIZE-1);
		t2 = std::chrono::high_resolution_clock::now();
		duration[i][1] = std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count();

		if (!check_if_sorted(p, SAMPLE_SIZE)) std::cout << "wrong!" << std::endl;
		else std::cout << "ok" << std::endl;

		// restore backup
		for (int i = 0; i < SAMPLE_SIZE; i++)
		{
			arr[i] = arr_cp[i];
		}
	}


	
}

