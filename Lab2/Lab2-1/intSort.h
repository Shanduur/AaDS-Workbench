#pragma once


void swap(int* a, int* b);
int* shellsort(int* arr, int n);
int* quicksort(int* arr, int l, int r);
int* quicksort2(int* arr, int n);