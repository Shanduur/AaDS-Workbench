#include "intSort.h"

void swap(int* a, int* b)
{
	int* temp = a;
	a = b;
	b = temp;
}

int* shellsort(int* arr, int n)
{
	int x, j = 0;
	int h = 1;

	while (h < (n / 9))
	{
		h = 3 * h + 1;
	}

	while (h > 0)
	{
		for (int i = h + 1; i < n; i++) {
			x = arr[i];
			j = i;

			while ((j >= h + 1) && (x < arr[j - h]))
			{
				arr[j] = arr[j - h];
				j = j - h;
			}

			arr[j] = x;
		}
		h = h / 3;
	}

	return arr;
}

int* quicksort(int* arr, int l, int r)
{
	int s, pivot = 0;

	if (l < r)
	{
		pivot = arr[l];
		s = l;

		for (int i = l + 1; i < r; i++)
		{
			if (arr[i] < pivot)
			{
				s = s + 1;
				swap(&arr[s], &arr[i]);
			}
		}
		swap(&arr[s], &arr[l]);

		quicksort(arr, l, s - 1);
		quicksort(arr, s + 1, r);
	}

	return arr;
}

int* quicksort2(int* arr, int n)
{


	return arr;
}