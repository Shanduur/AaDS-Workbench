#include <iostream>
#include <sstream>
#include <chrono>
#include <string>
#include <fstream>
#include <streambuf>

#define _FLAG false

int cursor;
int m = 3;
int m2;
int dictionary_size = 20;
int offset;
int length;

double before, after, ratio;

std::stringstream stream;
std::stringstream results;

std::string dictionary;
std::string x;
std::string output;

std::string path = "C:\\Repos\\Data\\";

std::string file_names[12] = {
    "strings\\s1.txt", "strings\\s2.txt", "strings\\s3.txt",
    "strings\\s4.txt", "strings\\s5.txt", "strings\\s6.txt",
    "books\\b1.txt", "books\\b2.txt", "books\\b3.txt",
    "books\\b4.txt", "books\\b5.txt", "books\\b6.txt"
};


void compress(std::string s) {
    before = s.length();
    x = s.at(0);
    for (int i = 0; i < dictionary_size; i++)
        dictionary.append(x);

    cursor = 0;

    while (cursor < s.length())
    {
        m2 = s.substr(cursor, m).length();
        x = s.substr(cursor, m2);
        
        while (dictionary.find(x) == -1 && m2 > 1) 
        {
            m2--;
            x = s.substr(cursor, m2);
        }

        if (dictionary.find(x) != -1) 
        {

            offset = dictionary_size - 1 - dictionary.rfind(x);
            length = m2;
            dictionary.erase(0, length).append(x);

            if (_FLAG) {
                std::cout << "(0," << offset << ',' << length << ")";
            }

            stream << 0 << offset << length;
            cursor += length;
        }
        else
        {
            dictionary.erase(0, 1).append(x);

            if (_FLAG) {
                std::cout << "(1," << x << ')';
            }

            stream << 1 << x;
            cursor++;
        }
    }
}

int main()
{
    for (int i = 0; i < 12; i++)
    {
        std::cout << "\nIteration: " << i << std::endl;
        std::ifstream t(path + file_names[i]);
        std::string str((std::istreambuf_iterator<char>(t)), std::istreambuf_iterator<char>());

        auto time1 = std::chrono::system_clock::now();
        compress(str);
        auto time2 = std::chrono::system_clock::now();

        auto duration = time2 - time1;

        output = stream.str();
        after = output.length();

        ratio = 100 * before / after;
        results << "before:" << before << "\tafter:" << after
            << "\tratio:" << ratio << "%\tduration:" << duration.count() << '\n';
    }
    

    std::cout << "\n\n\n" << results.str() << "\n\n\n";
}